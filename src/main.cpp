#include <Arduino.h>
#include <ComponentFactory.h>
#include <Component/Component.h>
#include <Component/System.h>
#include <Constants/Constant.h>
#include <ESP8266WiFi.h>
#include <LittleFS.h>
#include <PubSubClient.h>
#include <Quantum.h>
#include <Setup/Setup.h>

// Connectivity
WiFiClient wifiClient;
PubSubClient *mqttClient = new PubSubClient(wifiClient);

// Quantum and initial Components
Quantum *q;
System *sys;

// Environmental settings
StaticJsonDocument<2048> env;

void callback(char *topic, byte *payload, unsigned int length)
{
    // Stringify parameter
    String topicString(topic);
    String payloadString;
    for (unsigned int i = 0; i < length; i++)
        payloadString += (char)payload[i];

    // Print message
    Serial.println("MQTT in topic=" + topicString + ";payload=" + payloadString);

    // Determines the topics depth
    int depth = 0;
    for (unsigned int i = 0; i < topicString.length(); i++)
        if (topicString.charAt(i) == '/')
            depth++;

    // Check that topic has a depth of 3
    if (depth == 3)
    {
        // Get receiver and type from topic
        for (int i = 0; i < 2; i++)
            topicString = topicString.substring(topicString.indexOf("/") +
                                                1); // -> subdevice/type

        // Determine destination topic (command or config)
        String type =
            topicString.substring(topicString.lastIndexOf("/") + 1); // -> type

        // Determine destination device
        String deviceName =
            topicString.substring(0, topicString.indexOf("/")); // -> subdevice

        // Parse json from payload
        StaticJsonDocument<128> doc;
        DeserializationError err = deserializeJson(doc, payloadString);

        // Check if json parsing succeeded
        if (err == OK && doc.is<JsonObject>())
        {
            // Forward json to destination type
            JsonObject obj = doc.as<JsonObject>();
            if (type == Constant::MQTT_COMMAND_TYPE)
            {
                q->command(deviceName, obj);
                Serial.println("Commanded name=" + deviceName);
            }
            else if (type == Constant::MQTT_CONFIG_TYPE)
            {
                q->configure(deviceName, obj);
                Serial.println("Configured name=" + deviceName);
            }
        }
    }
}

void tryConnect()
{
    // Check if already connected or smartmode disabled
    if ((mqttClient->connected() && WiFi.status() == WL_CONNECTED))
        return;

    if (!mqttClient->connected() && WiFi.status() == WL_CONNECTED)
    {
        delete mqttClient;
        mqttClient = new PubSubClient(wifiClient);

        String ip = env[Constant::MQTT_KEY][Constant::IP_KEY];
        int port = env[Constant::MQTT_KEY][Constant::PORT_KEY];

        mqttClient->setServer(ip.c_str(), port);
        mqttClient->setCallback(callback);
        q->setPsc(mqttClient);

        Serial.print("MQTT connection initiated");
        if (mqttClient->connect(String(ESP.getChipId()).c_str()))
            mqttClient->subscribe((q->getBaseTopic() + "#").c_str());
        Serial.println("!");
    }
}

void setup()
{
    // Start serial communication
    Serial.begin(9600);
    Serial.println();

    // Check for reset pin
    pinMode(D7, INPUT);
    if (digitalRead(D7) == LOW)
    {
        Serial.println("Formating...");
        LittleFS.format();
        Serial.println("Remove jumper and press reset!");
        while (true)
        {
            delay(500);
            yield();
        }
    }

    int titleLength = 11 + Constant::SOFTWARE_NAME.length();
    Serial.println("+ " + Constant::SOFTWARE_NAME + " ------- +");

    Serial.print("|");
    for (int i = 1; i < titleLength; i++)
        Serial.print(" ");
    Serial.println("|");

    Serial.print("| VERSION " + Constant::VERSION_FLOAT);
    for (int i = 10 + Constant::VERSION_FLOAT.length(); i < titleLength; i++)
        Serial.print(" ");
    Serial.println("|");

    Serial.print("| CODE    " + Constant::VERSION_CODENAME);
    for (int i = 10 + Constant::VERSION_CODENAME.length(); i < titleLength; i++)
        Serial.print(" ");
    Serial.println("|");

    Serial.print("+ ");
    for (int i = 3; i < titleLength; i++)
        Serial.print("-");
    Serial.println(" +");

    // Begin SPIFFS
    if (!LittleFS.begin())
    {
        if (!LittleFS.begin())
        {
            Serial.println("SPIFFS formatted. Restart in 5 sec.");
            delay(5000);
            ESP.restart();
        }
    }

    // Load environmental config
    File envConfig = LittleFS.open("env.json", "r");
    deserializeJson(env, envConfig);
    envConfig.close();

    // Check if env config contains sufficient data
    if (!env[Constant::WIFI_KEY][Constant::SSID_KEY].is<String>() || !env[Constant::WIFI_KEY][Constant::PASSWORD_KEY].is<String>() || !env[Constant::NAME_KEY].is<String>() || !env[Constant::MQTT_KEY][Constant::IP_KEY].is<String>() || !env[Constant::MQTT_KEY][Constant::PORT_KEY].is<int>())
    {
        // Start setup if not configured yet
        Setup s(Constant::SOFTWARE_NAME);

        while (!s.loopSetup())
        {
            delay(10);
        }

        env[Constant::WIFI_KEY][Constant::SSID_KEY] = s.getSSID();
        env[Constant::WIFI_KEY][Constant::PASSWORD_KEY] = s.getPassword();
        env[Constant::NAME_KEY] = s.getDeviceName();
        env[Constant::MQTT_KEY][Constant::IP_KEY] = s.getMQTTIp();
        env[Constant::MQTT_KEY][Constant::PORT_KEY] = s.getMQTTPort();

        // Save config
        File wConfig = LittleFS.open("env.json", "w");
        serializeJson(env, wConfig);
        wConfig.close();

        ESP.restart();
    }

    // Instantiate Quantum
    q = new Quantum(env[Constant::NAME_KEY], Constant::SOFTWARE_NAME, Constant::COMPONENTS_FILE, mqttClient);

    // Connect WiFi and MQTT
    WiFi.begin(env[Constant::WIFI_KEY][Constant::SSID_KEY].as<String>(), env[Constant::WIFI_KEY][Constant::PASSWORD_KEY].as<String>());
    //connect();

    // Register coordinator
    sys = new System(q);
    q->add(sys);
}

void loop()
{
    tryConnect();
    if (mqttClient != nullptr && WiFi.status() == WL_CONNECTED && mqttClient->connected())
        mqttClient->loop();
    q->loop();
}