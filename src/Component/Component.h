#pragma once
#include <ArduinoJson.h>
#include <Constants/Constant.h>
#include <LittleFS.h>

class Component
{
private:
    // Identification
    String name;
    String type;

    // Configuration
    JsonDocument *config;
    String configPath;

    // Event called if config received an update
    virtual void configUpdated() = 0;

    // Writes config to SPIFFS
    void writeConfig();

public:
    Component(String name, String type);

    String getName();
    String getType();
    JsonDocument &getConfig();

    // If true is returned, getResult() will be called to retrieve the result.
    virtual bool isResultReady() = 0;

    // Called if isResultReady() returned true
    virtual JsonObject getResult() = 0;

    // Called every tick
    virtual void loop() = 0;

    // Called if component receives a command via MQTT
    virtual void command(JsonObject &command) = 0;

    // Config methods
    void configure(JsonObject &config);
    void clearConfig();
};