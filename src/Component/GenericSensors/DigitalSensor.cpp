#include <Component/GenericSensors/DigitalSensor.h>

DigitalSensor::DigitalSensor(String name) : Component(name, Constant::DIGITAL_SENSOR_TYPE)
{
    configUpdated();
}

void DigitalSensor::configUpdated()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
        pinMode(getConfig()[Constant::PIN_KEY], INPUT);
}

bool DigitalSensor::isResultReady()
{
    return state[Constant::CHANGED_KEY];
}

JsonObject DigitalSensor::getResult()
{
    state[Constant::CHANGED_KEY] = false;
    return state.as<JsonObject>();
}

void DigitalSensor::loop()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
    {
        // Read value
        bool value = digitalRead(getConfig()[Constant::PIN_KEY]);
        if (state[Constant::LAST_KEY][Constant::VALUE_KEY] != value)
        {
            // Update state
            state[Constant::LAST_KEY][Constant::VALUE_KEY] = state[Constant::VALUE_KEY];
            state[Constant::VALUE_KEY] = value;
            state[Constant::CHANGED_KEY] = true;
        }
    }
}

void DigitalSensor::command(JsonObject &command)
{
}