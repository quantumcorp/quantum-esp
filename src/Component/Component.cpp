#include <Component/Component.h>

Component::Component(String name, String type) : name(name), configPath(name + Constant::GENERAL_CONFIG_FILE_ENDING), type(type)
{
    // Parse json from config file
    config = new StaticJsonDocument<2048>();
    File configFile = LittleFS.open(configPath, "r");
    deserializeJson(*config, configFile);
    configFile.close();

    // Print debug output
    Serial.println("Constructed name=" + name + ";type=" + type);
}

void Component::writeConfig()
{
    // Serialize configureation to corresponding config file
    File cfg = LittleFS.open(configPath, "w");
    serializeJson(*config, cfg);

    // Debug output
    Serial.print("Wrote config name=" + name + ";type=" + type);
    serializeJson(*config, Serial);
    Serial.println();

    // Close file
    cfg.close();
}

String Component::getName()
{
    return name;
}

String Component::getType()
{
    return type;
}

JsonDocument &Component::getConfig()
{
    return *config;
}

void Component::configure(JsonObject &cfg)
{
    // Debug output
    Serial.println("Received configuration name=" + name + ";type=" + type);

    int valsChanged = 0;
    for (const auto &val : cfg)
    {
        // Check if value would be different
        if (config->getOrAddMember(val.key()) != val.value())
            valsChanged++;

        // Overwrite or create value
        config->getOrAddMember(val.key()).set(val.value());
    }

    // Call event and flash config if values were changed
    if (valsChanged > 0)
    {
        writeConfig();
        configUpdated();
    }
}

void Component::clearConfig()
{
    config = new StaticJsonDocument<2048>();
    writeConfig();
}