#include <Arduino.h>
#include <Constants/Constant.h>
#include <ComponentFactory.h>
#include <Component/Component.h>
#include <FS.h>
#include <Quantum.h>

class System : public Component
{
private:
    // Quantum instance which gets controlled by this
    Quantum &q;
    
    StaticJsonDocument<1024> versionInfo;
    bool startup = true;

    virtual void configUpdated();

public:
    System(Quantum *q);

    bool isResultReady();
    JsonObject getResult();

    void loop();
    void command(JsonObject &command);
};