#include <Constants/Constant.h>

//                          __               __
//  _____________  ____    |__| ____   _____/  |_    ______
//  \____ \_  __ \/  _ \   |  |/ __ \_/ ___\   __\  / ____/
//  |  |_> >  | \(  <_> )  |  \  ___/\  \___|  |   < <_|  |
//  |   __/|__|   \____/\__|  |\___  >\___  >__|    \__   |
//  |__|               \______|    \/     \/           |__|
//
//  by epileptic

//// General
// Software information
const String Constant::SOFTWARE_NAME = "quantum-esp";
const String Constant::VERSION_CODENAME = "vanilla";
const String Constant::VERSION_FLOAT = "0.93";
const String Constant::VERSION_SLUG = SOFTWARE_NAME + "_V" + VERSION_FLOAT + "_" + VERSION_CODENAME;
const String Constant::DEV_NAME = "epileptic";

// Configuration
const String Constant::GENERAL_CONFIG_FILE_ENDING = ".json";
const String Constant::COMPONENTS_FILE = "components.json";
const String Constant::SYSTEM_SUBDEVICE_NAME = "system";

//// Types
// Component
const String Constant::SYSTEM_TYPE = "system";
const String Constant::NEOPIXEL_TYPE = "neopixel";
const String Constant::ANALOG_SENSOR_TYPE = "analogsensor";
const String Constant::DIGITAL_SENSOR_TYPE = "digitalsensor";
const String Constant::ANALOG_ACTOR_TYPE = "analogactor";
const String Constant::DIGITAL_ACTOR_TYPE = "digitalactor";
const String Constant::ONEWIRE_TYPE = "genericow";
const String Constant::DALLAS_TEMP_TYPE = "dallastemp";
const String Constant::SERIAL_DEBUG_TYPE = "serialdebug";

// MQTT
const String Constant::MQTT_COMMAND_TYPE = "command";
const String Constant::MQTT_CONFIG_TYPE = "config";
const String Constant::MQTT_SENSOR_SUBTOPIC = "output";

//// Keys
// Command
const String Constant::CLEAR_CONFIG_KEY = "clearconfig";
const String Constant::CLEAR_SPIFFS_KEY = "clearspiffs";

// General
const String Constant::COLOR_KEY = "color";
const String Constant::AMOUNT_KEY = "amount";
const String Constant::PIN_KEY = "pin";
const String Constant::ADD_KEY = "add";
const String Constant::REMOVE_KEY = "remove";
const String Constant::TYPE_KEY = "type";
const String Constant::NAME_KEY = "name";
const String Constant::MODE_KEY = "mode";
const String Constant::TIME_KEY = "time";
const String Constant::SUSPEND_KEY = "suspend";
const String Constant::VALUE_KEY = "value";
const String Constant::CHANGED_KEY = "changed";
const String Constant::WIFI_KEY = "wifi";
const String Constant::SSID_KEY = "ssid";
const String Constant::PASSWORD_KEY = "pwd";
const String Constant::MQTT_KEY = "mqtt";
const String Constant::IP_KEY = "ip";
const String Constant::PORT_KEY = "port";
const String Constant::STATE_KEY = "state";
const String Constant::COLOR_R_KEY = "r";
const String Constant::COLOR_G_KEY = "g";
const String Constant::COLOR_B_KEY = "b";

// Neopixel
const String Constant::NEOPIXEL_MODE_OFF = "off";
const String Constant::NEOPIXEL_MODE_COLOR = "color";
const String Constant::NEOPIXEL_MODE_RAINBOW = "rainbow";
const String Constant::NEOPIXEL_MODE_FADE = "fade";
const String Constant::NEOPIXEL_MODE_CHASE = "chase";

// Operator
const String Constant::LAST_KEY = "last";
const String Constant::KEY_CONNECTOR = "_";
const String Constant::LAST_PREFIX = LAST_KEY + KEY_CONNECTOR;

//// Defaults
// Neopixel
const String Constant::NEOPIXEL_DEFAULT_MODE = "off";
const long Constant::NEOPIXEL_DEFAULT_TIME = 10;
const bool Constant::NEOPIXEL_DEFAULT_SUSPEND = false;

const int Constant::NEOPIXEL_DEFAULT_COLOR_R = 199;
const int Constant::NEOPIXEL_DEFAULT_COLOR_G = 255;
const int Constant::NEOPIXEL_DEFAULT_COLOR_B = 191;