#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include <Component/Component.h>
#include <Helper/JsonHelper.h>

class DigitalActor : public Component
{
private:
    void configUpdated();

    // State
    StaticJsonDocument<128> state;

public:
    DigitalActor(String name);

    bool isResultReady();
    JsonObject getResult();

    void loop();
    void command(JsonObject &command);
};