#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>

class JsonHelper
{
public:
    template <typename T>
    static T vor(JsonDocument &subject, String key, T defaultValue)
    {
        if (subject[key].is<T>())
        {
            return subject[key];
        }
        else
        {
            subject[key] = defaultValue;
            return defaultValue;
        }
    }

    template <typename T>
    static T vor(JsonDocument &subject, JsonDocument &config, String key, T defaultValue)
    {
        if (subject[key].is<T>())
        {
            return subject[key];
        }
        else
        {
            if (config[key].is<T>())
            {
                subject[key] = config[key];
            }
            else
            {
                subject[key] = defaultValue;
            }
            return defaultValue;
        }
    }

    static void merge(JsonDocument &doc, JsonObject &obj)
    {
        for (const auto &val : obj)
            doc[val.key()] = val.value();
    }
};