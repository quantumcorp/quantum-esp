#include <Arduino.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>

class Setup
{
private:
    String head = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/></head><body>";

    String contentPre = "<h1>First Run Setup</h1><h2>General</h2><form action=\"/submit\" method=\"POST\"><h3>Enter Device Name:</h3><input type=\"text\" required=\"true\" placeholder=\"Device Name\" name=\"devicename\" /><h2>MQTT</h2><h3>Enter Server IP:</h3><input type=\"text\" required=\"true\" placeholder=\"MQTT Server IP\" name=\"mqttip\"/><h3>Enter Server Port<h3/><input type=\"number\" required=\"true\" placeholder=\"MQTT Server Port\" name=\"mqttport\"/><h2>WiFi</h2><h3>Enter WiFi Password:</h3><input type=\"password\" placeholder=\"Password\" name=\"password\" /><h3>Select WiFi:</h3><p>Refresh page if WiFi is not listed</p>";

    String ssidButtonStart = "<input type=\"submit\" name=\"ssid\" value=\"";
    String ssidButtonEnd = "\" /><br>";

    String contentPost = "</form>";

    String invalidPassword = "<p class=\"error animated shake\">Invalid Password</p>";

    String foot = "</body></html>";

    String style = "<style>html {background-color: #222222;color: #c7d3bf;font-family: sans-serif;}body {text-align: center;font-family: sans-serif;}p {margin: 0px;padding: 0px;}h2,h3 {margin: 2px;padding: 2px;}h2 {margin-top: 20px;}input {padding: 15px;margin: 10px;width: 90%;background-color: #c7d3bf;color: #222222;font-weight: 500;border-color: #c7d3bf;border-style: solid;-o-transition: .4s;-ms-transition: .4s;-moz-transition: .4s;-webkit-transition: .4s;transition: .4s;}input:hover {background-color: #222222;color: #c7d3bf;}.error {color: #ffa0a8;-o-transition: .4s;-ms-transition: .4s;-moz-transition: .4s;-webkit-transition: .4s;transition: .4s;}.error:hover {color: #c7d3bf;}</style>";

    DNSServer *dnsServer;
    ESP8266WebServer *webServer;

    String deviceName;
    String ssid;
    String password;
    
    String mqttIp;
    int mqttPort;
    
    bool set = false;

    void handle();
    void passwordSubmited();

public:
    Setup(String serviceName);

    bool loopSetup();

    String getDeviceName();
    String getSSID();
    String getPassword();
    
    String getMQTTIp();
    int getMQTTPort();
};