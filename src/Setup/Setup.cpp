#include <Setup/Setup.h>

Setup::Setup(String serviceName)
{
    // Configure WiFi module to be hostpot and station at once
    WiFi.mode(WIFI_AP_STA);

    // Configure WiFi hotspot
    IPAddress apIP(172, 217, 28, 1);
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    WiFi.softAP(serviceName + " " + String(ESP.getChipId()));

    // Redirect whole DNS traffic to AP ip
    dnsServer = new DNSServer();
    dnsServer->start(53, "*", apIP);

    //// Setup WebServer to deliver login page to client
    // Instantiate webserver
    webServer = new ESP8266WebServer(80);

    // Add handler if requested subpage not found
    webServer->on("/", std::bind(&Setup::handle, this));
    webServer->onNotFound(std::bind(&Setup::handle, this));

    // Add handler for password submission
    webServer->on("/submit", HTTP_POST, std::bind(&Setup::passwordSubmited, this));

    // Start webserver
    webServer->begin();
}

void Setup::handle()
{
    //// Build HTML document
    // HTML HEAD
    String html = head + contentPre;

    //// Add WiFi network submit buttons
    // Scan WiFi networks
    int amount = WiFi.scanNetworks();
    for (int i = 0; i < amount; i++)
        html += ssidButtonStart + WiFi.SSID(i) + ssidButtonEnd;

    // Add html foot
    html += contentPost;
    if (webServer->hasArg("failed"))
        html += invalidPassword;
    html += foot + style;

    // Deliver webpage as HTML
    webServer->send(200, "text/html", html);
}

void Setup::passwordSubmited()
{
    // Extract post arguments
    deviceName = webServer->arg("devicename");
    ssid = webServer->arg("ssid");
    password = webServer->arg("password");

    mqttIp = webServer->arg("mqttip");
    mqttPort = atoi(webServer->arg("mqttport").c_str());

    Serial.println(deviceName);
    Serial.println(ssid);
    Serial.println(password);
    Serial.println(mqttIp);
    Serial.println(mqttPort);

    WiFi.begin(ssid, password);

    if (WiFi.waitForConnectResult() == WL_CONNECTED)
    {
        Serial.println("Connected");
        // Show success page
        webServer->send(200, "text/html", head + "<h1>Success!</h1><br><p>Rebooting...<p/>" + foot + style);

        // Wait a second and kill access point
        delay(1000);
        WiFi.mode(WIFI_STA);

        // Make loop method return true
        set = true;
    }
    else
    {
        Serial.println("Not Connected");
        // Redirect to root page with Invalid Password paragraph
        webServer->sendHeader("Location", "/?failed=1", true);
        webServer->send(302, "text/plain", "");
    }
}

bool Setup::loopSetup()
{
    dnsServer->processNextRequest();
    webServer->handleClient();

    return set;
}

String Setup::getDeviceName()
{
    return deviceName;
}

String Setup::getSSID()
{
    return ssid;
}

String Setup::getPassword()
{
    return password;
}

String Setup::getMQTTIp()
{
    return mqttIp;
}

int Setup::getMQTTPort()
{
    return mqttPort;
}
