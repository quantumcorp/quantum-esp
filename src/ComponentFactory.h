#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include <Constants/Constant.h>
#include <Component/Neopixel.h>
#include <Component/Component.h>
#include <Component/GenericActors/AnalogActor.h>
#include <Component/GenericActors/DigitalActor.h>
#include <Component/GenericSensors/AnalogSensor.h>
#include <Component/GenericSensors/DigitalSensor.h>

class ComponentFactory
{
public:
    static Component *construct(JsonObject &description);
};