#include <Component/Neopixel.h>

Neopixel::Neopixel(String name) : Component(name, Constant::NEOPIXEL_TYPE)
{
    configUpdated();
}

bool Neopixel::isResultReady()
{
    return false;
}

JsonObject Neopixel::getResult()
{
    return JsonObject();
}

void Neopixel::loop()
{
    // Check if strip is not nullptr
    if (strip != NULL)
    {
        bool suspend = JsonHelper::vor(state, Constant::SUSPEND_KEY, Constant::NEOPIXEL_DEFAULT_SUSPEND);

        // Check if suspended
        if (suspend)
        {
            strip->clear();
            strip->show();
        }
        else
        {
            // Get mode
            String mode = JsonHelper::vor(state, getConfig(), Constant::MODE_KEY, Constant::NEOPIXEL_DEFAULT_MODE);

            if (mode == Constant::NEOPIXEL_MODE_OFF)
            {
                strip->clear();
                strip->show();
                delay(500);
            }
            else if (mode == Constant::NEOPIXEL_MODE_RAINBOW)
            {
                // Check if hue in bounds
                if (hue < 5 * 65536)
                {
                    hue += 256;
                }
                else
                {
                    hue = 0;
                }

                // Iterate over pixels
                for (int i = 0; i < strip->numPixels(); i++)
                {
                    // Calculate individial hue
                    int pixelHue = hue + (i * 65536L / strip->numPixels());

                    // Set pixel color
                    strip->setPixelColor(i, strip->gamma32(strip->ColorHSV(pixelHue)));
                }

                //  Show changes
                strip->show();

                // Load delay and show
                String timeKey = Constant::NEOPIXEL_MODE_RAINBOW + Constant::KEY_CONNECTOR + Constant::TIME_KEY;
                delay(JsonHelper::vor(state, getConfig(), timeKey, Constant::NEOPIXEL_DEFAULT_TIME));
            }
            else if (mode == Constant::NEOPIXEL_MODE_FADE)
            {
                // Check if hue in bounds
                if (hue < 5 * 65536)
                {
                    hue += 256;
                }
                else
                {
                    hue = 0;
                }

                // Update colors and show changes
                strip->fill(strip->gamma32(strip->ColorHSV(hue)), 0, strip->numPixels());
                strip->show();

                // Load delay and show
                String timeKey = Constant::NEOPIXEL_MODE_FADE + Constant::KEY_CONNECTOR + Constant::TIME_KEY;
                delay(JsonHelper::vor(state, timeKey, Constant::NEOPIXEL_DEFAULT_TIME));
            }
            else if (mode == Constant::NEOPIXEL_MODE_CHASE)
            {
            }
            else if (mode = Constant::NEOPIXEL_MODE_COLOR)
            {
                int r = JsonHelper::vor(state, getConfig(), Constant::COLOR_R_KEY, Constant::NEOPIXEL_DEFAULT_COLOR_R); 
                int g = JsonHelper::vor(state, getConfig(), Constant::COLOR_G_KEY, Constant::NEOPIXEL_DEFAULT_COLOR_G);
                int b = JsonHelper::vor(state, getConfig(), Constant::COLOR_B_KEY, Constant::NEOPIXEL_DEFAULT_COLOR_B);

                strip->fill(strip->Color(r, g, b), 0, strip->numPixels());
                strip->show();
                delay(500);
            }
        }
    }
}

void Neopixel::command(JsonObject &command)
{
    JsonHelper::merge(state, command);
}

void Neopixel::configUpdated()
{
    if (getConfig()[Constant::PIN_KEY].is<int>() && getConfig()[Constant::AMOUNT_KEY].is<int>())
    {
        strip = new Adafruit_NeoPixel(getConfig()[Constant::AMOUNT_KEY], getConfig()[Constant::PIN_KEY], NEO_KHZ400 + NEO_GRB);
        strip->begin();
        strip->clear();
        strip->show();
    }
    else
    {
        strip = NULL;
    }
}