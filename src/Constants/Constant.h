#pragma once
#include <Arduino.h>

class Constant
{
private:
    Constant();

public:
    //// General
    // Software info
    static const String SOFTWARE_NAME;
    static const String VERSION_CODENAME;
    static const String VERSION_FLOAT;
    static const String VERSION_SLUG;
    static const String DEV_NAME;

    // Configuration
    static const String GENERAL_CONFIG_FILE_ENDING;
    static const String COMPONENTS_FILE;
    static const String SYSTEM_SUBDEVICE_NAME;

    //// Types
    // Component
    static const String SYSTEM_TYPE;
    static const String NEOPIXEL_TYPE;
    static const String ANALOG_SENSOR_TYPE;
    static const String DIGITAL_SENSOR_TYPE;
    static const String ANALOG_ACTOR_TYPE;
    static const String DIGITAL_ACTOR_TYPE;
    static const String ONEWIRE_TYPE;
    static const String DALLAS_TEMP_TYPE;
    static const String SERIAL_DEBUG_TYPE;

    // MQTT
    static const String MQTT_COMMAND_TYPE;
    static const String MQTT_CONFIG_TYPE;
    static const String MQTT_SENSOR_SUBTOPIC;

    //// Keys
    // Command
    static const String CLEAR_CONFIG_KEY;
    static const String CLEAR_SPIFFS_KEY;

    // General
    static const String COLOR_KEY;
    static const String AMOUNT_KEY;
    static const String PIN_KEY;
    static const String ADD_KEY;
    static const String REMOVE_KEY;
    static const String TYPE_KEY;
    static const String NAME_KEY;
    static const String MODE_KEY;
    static const String TIME_KEY;
    static const String SUSPEND_KEY;
    static const String VALUE_KEY;
    static const String CHANGED_KEY;
    static const String WIFI_KEY;
    static const String SSID_KEY;
    static const String PASSWORD_KEY;
    static const String MQTT_KEY;
    static const String IP_KEY;
    static const String PORT_KEY;
    static const String STATE_KEY;
    static const String COLOR_R_KEY;
    static const String COLOR_G_KEY;
    static const String COLOR_B_KEY;

    // Neopixel
    static const String NEOPIXEL_MODE_OFF;
    static const String NEOPIXEL_MODE_COLOR;
    static const String NEOPIXEL_MODE_RAINBOW;
    static const String NEOPIXEL_MODE_FADE;
    static const String NEOPIXEL_MODE_CHASE;

    // Operator
    static const String LAST_KEY;
    static const String KEY_CONNECTOR;
    static const String LAST_PREFIX;

    //// Defaults
    // Neopixel
    static const String NEOPIXEL_DEFAULT_MODE;
    static const long NEOPIXEL_DEFAULT_TIME;
    static const bool NEOPIXEL_DEFAULT_SUSPEND;

    static const int NEOPIXEL_DEFAULT_COLOR_R;
    static const int NEOPIXEL_DEFAULT_COLOR_G;
    static const int NEOPIXEL_DEFAULT_COLOR_B;
};