#include <Quantum.h>

Quantum::Quantum(String deviceName, String serviceName, String configurationPath, PubSubClient *psc)
{
    setPsc(psc);

    // Load components file
    File cfg = LittleFS.open(Constant::COMPONENTS_FILE, "r");
    config = new StaticJsonDocument<2048>;
    deserializeJson(*config, cfg);
    cfg.close();
    Serial.println("Loaded quantum components filepath=" + configurationPath);

    // Regsister components described in components file
    if (config->is<JsonArray>() && config->as<JsonArray>().size() > 0)
    {
        JsonArray docArr = config->as<JsonArray>();
        for (unsigned int i = 0; i < docArr.size(); i++)
        {
            if (docArr[i].is<JsonObject>())
            {
                JsonObject obj = docArr[i];
                Component *construction = ComponentFactory::construct(obj);
                if (construction)
                    add(construction);
            }
        }
    }

    baseTopic = serviceName + "/" + deviceName + "/";
}

Component *Quantum::find(String name)
{
    for (const auto &value : components)
        if (name == value->getName())
            return value;
    return nullptr;
}

void Quantum::writeConfig()
{
    File cfg = LittleFS.open(Constant::COMPONENTS_FILE, "w");
    serializeJson(*config, cfg);
    cfg.close();
    Serial.println("Wrote config filepath=" + Constant::COMPONENTS_FILE);
}

String Quantum::getBaseTopic()
{
    return baseTopic;
}

bool Quantum::containsName(String name)
{
    if (find(name) == nullptr)
        return false;
    return true;
}

void Quantum::setPsc(PubSubClient *psc)
{
    mqttClient = psc;
}

void Quantum::registerAdd(Component *component)
{
    Serial.println("Register component name=" + component->getName());

    // Check that component is not registered yet
    if (component && find(component->getName()) == nullptr)
    {
        // Build json object for declaration file
        StaticJsonDocument<2084> add;
        add[Constant::NAME_KEY] = component->getName();
        add[Constant::TYPE_KEY] = component->getType();

        // Add component to declaration json
        config->add(add);

        // Write declaration file
        writeConfig();

        // Add component to quantum
        Quantum::add(component);
    }
}

void Quantum::add(Component *component)
{
    Serial.println("Added component name=" + component->getName());
    if (component && find(component->getName()) == nullptr)
        components.push_back(component);
}

void Quantum::remove(String name)
{
    JsonArray confAsArray = config->as<JsonArray>();
    for (unsigned int i = 0; i < confAsArray.size(); i++)
    {
        if (confAsArray[i][Constant::NAME_KEY] == name)
            config->remove(i);
    }

    clearConfig(name);
    auto sj = std::find(components.begin(), components.end(), find(name));
    if (sj != components.end())
        components.erase(sj);
    writeConfig();
}

void Quantum::command(String name, JsonObject &command)
{
    Component *c;
    if ((c = find(name)) != nullptr)
        c->command(command);
}

void Quantum::configure(String name, JsonObject &config)
{
    Component *c;
    if ((c = find(name)) != nullptr)
        c->configure(config);
}

void Quantum::clearConfig(String name)
{
    Component *c;
    if ((c = find(name)) != nullptr)
        c->clearConfig();
}

void Quantum::loop()
{
    for (auto const &value : components)
    {
        value->loop();
        if (mqttClient != NULL && WiFi.status() == WL_CONNECTED && mqttClient->connected() && value->isResultReady())
        {
            String topic = getBaseTopic() + value->getName() + "/" + Constant::MQTT_SENSOR_SUBTOPIC;

            String jsonString;
            serializeJson(value->getResult(), jsonString);

            mqttClient->publish(topic.c_str(), jsonString.c_str());
        }
    }
}