#include <Component/GenericActors/DigitalActor.h>

DigitalActor::DigitalActor(String name) : Component(name, Constant::DIGITAL_ACTOR_TYPE)
{
    configUpdated();
}

void DigitalActor::configUpdated()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
        pinMode(getConfig()[Constant::PIN_KEY], OUTPUT);

    if (getConfig()[Constant::STATE_KEY].is<bool>())
        state[Constant::STATE_KEY] = getConfig()[Constant::STATE_KEY];
}

bool DigitalActor::isResultReady()
{
    return false;
}

JsonObject DigitalActor::getResult()
{
    return JsonObject();
}

void DigitalActor::loop()
{
    // Write actor value if changed
    if (state[Constant::STATE_KEY].is<bool>() && state[Constant::STATE_KEY] != state[Constant::LAST_KEY][Constant::STATE_KEY])
        digitalWrite(getConfig()[Constant::PIN_KEY], state[Constant::STATE_KEY]);
    state[Constant::LAST_KEY][Constant::STATE_KEY] = state[Constant::STATE_KEY];
}

void DigitalActor::command(JsonObject &command)
{
    JsonHelper::merge(state, command);
}