#include <Component/GenericSensors/AnalogSensor.h>

AnalogSensor::AnalogSensor(String name) : Component(name, Constant::ANALOG_SENSOR_TYPE)
{
    configUpdated();
}

void AnalogSensor::configUpdated()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
        pinMode(getConfig()[Constant::PIN_KEY], INPUT);
}

bool AnalogSensor::isResultReady()
{
    return state[Constant::CHANGED_KEY];
}

JsonObject AnalogSensor::getResult()
{
    state[Constant::CHANGED_KEY] = false;
    return state.as<JsonObject>();
}

void AnalogSensor::loop()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
    {
        // Read value
        int value = analogRead(getConfig()[Constant::PIN_KEY]);
        if (state[Constant::LAST_KEY][Constant::VALUE_KEY] != value)
        {
            // Update state
            state[Constant::LAST_KEY][Constant::VALUE_KEY] = state[Constant::VALUE_KEY];
            state[Constant::VALUE_KEY] = value;
            state[Constant::CHANGED_KEY] = true;
        }
    }
}

void AnalogSensor::command(JsonObject &command)
{
}