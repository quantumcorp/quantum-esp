#include <ComponentFactory.h>

Component *ComponentFactory::construct(JsonObject &description)
{
    // Check if file is valid
    bool validObject = true;
    validObject = validObject && description[Constant::TYPE_KEY].is<String>();
    validObject = validObject && description[Constant::NAME_KEY].is<String>();

    Component *construction = nullptr;
    if (validObject)
    {
        Serial.println("Construct...");

        String name = description[Constant::NAME_KEY];
        String type = description[Constant::TYPE_KEY];

        if (type == Constant::NEOPIXEL_TYPE)
            construction = new Neopixel(name);
        else if (type == Constant::ANALOG_SENSOR_TYPE)
            construction = new AnalogSensor(name);
        else if (type == Constant::DIGITAL_SENSOR_TYPE)
            construction = new DigitalSensor(name);
        else if (type == Constant::ANALOG_ACTOR_TYPE)
            construction = new AnalogActor(name);
        else if (type == Constant::DIGITAL_ACTOR_TYPE)
            construction = new DigitalActor(name);
    }
    return construction;
}