#pragma once
#include <Arduino.h>
#include <ComponentFactory.h>
#include <Component/Component.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

class Quantum
{
private:
    JsonDocument *config;
    String baseTopic;
    std::vector<Component *> components;
    Component *find(String name);

    // MQTT
    PubSubClient *mqttClient;

    void writeConfig();

public:
    Quantum(String deviceName, String serviceName, String configurationPath, PubSubClient *psc);

    String getBaseTopic();
    bool containsName(String name);

    void setPsc(PubSubClient *psc);

    // Returns base topic
    void registerAdd(Component *component);
    void add(Component *component);
    void remove(String name);

    void command(String name, JsonObject &command);
    void configure(String name, JsonObject &config);
    void clearConfig(String name);

    void loop();
};