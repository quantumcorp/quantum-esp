<h1>QUANTUM-ESP</h1>
Quantum-ESP is part of "Project q" which aims to create a simple diy 
smarthome ecosystem based on opensource hardware and software. Quantum-ESP is
the client firmware for ESP8266 based microcontrollers.

<h2>Requirements</h2>
You need a WiFi and a MQTT server connected to your network.   

<h2>How to use Quantum-ESP?</h2>
The best way is to download the sources and compile it yourself. With this method
you are able to compile it for any controller you want. 

1. Download Visual Studio Code (https://code.visualstudio.com/)  
2. Open Visual Studio Code and install the Plugin PlatformIO (https://platformio.org/). Follow the installation instructions given by PlatformIO
3. Download this software repository and open it with PlatformIO: 
    1. Go to https://gitlab.com/epileptic/quantum-esp
    2. Click Download>zip
    3. Unzip the downloaded code
    4. Got to Visual Studio Code and click open in the "PIO Home" tab and open the unzipped project
4. Go back to "PIO Home" and click "Libraries"
5. Type in "ArduinoJson" and install "ArduinoJson" by Benoit Blanchon
6. Type in "Adafruit NeoPixel" and install "Adafruit NeoPixel" by Adafruit
7. Type in "PubSubClient" and install "PubSubClient" by Nick O'Leary
8. Open the main.cpp of the opened project
9. Connect your board and press the little arrow in the bottom toolbar
10. If you are using a different board than the "Wemos D1 mini" go to https://platformio.org/boards search for your board and replace the current configuration in "quantum-esp > platform.ini" by the configuration for your board