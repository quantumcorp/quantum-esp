#include <Component/GenericActors/AnalogActor.h>

AnalogActor::AnalogActor(String name) : Component(name, Constant::ANALOG_ACTOR_TYPE)
{
    configUpdated();
}

void AnalogActor::configUpdated()
{
    if (getConfig()[Constant::PIN_KEY].is<int>())
        pinMode(getConfig()[Constant::PIN_KEY], OUTPUT);

    if (getConfig()[Constant::STATE_KEY].is<int>())
        state[Constant::STATE_KEY] = getConfig()[Constant::STATE_KEY];
}

bool AnalogActor::isResultReady()
{
    return false;
}

JsonObject AnalogActor::getResult()
{
    return JsonObject();
}

void AnalogActor::loop()
{
    // Write actor value if changed
    if (state[Constant::STATE_KEY].is<int>() && state[Constant::STATE_KEY].is<int>() != state[Constant::LAST_KEY][Constant::STATE_KEY])
        analogWrite(getConfig()[Constant::PIN_KEY], state[Constant::STATE_KEY]);
    state[Constant::LAST_KEY][Constant::STATE_KEY] = state[Constant::STATE_KEY];
}

void AnalogActor::command(JsonObject &command)
{
    JsonHelper::merge(state, command);
}