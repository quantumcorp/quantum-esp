#include <Component/System.h>

System::System(Quantum *quantum) : Component(Constant::SYSTEM_SUBDEVICE_NAME, Constant::SYSTEM_TYPE), q(*quantum)
{
}

bool System::isResultReady()
{
    if (startup)
    {
        startup = false;
        return true;
    }

    return false;
}

JsonObject System::getResult()
{
    versionInfo["sdc"] = ESP.getResetReason();
    versionInfo["v"] = Constant::VERSION_FLOAT;

    return versionInfo.as<JsonObject>();
}

void System::loop()
{
}

void System::command(JsonObject &command)
{
    // System add command
    if (command[Constant::ADD_KEY].is<JsonObject>())
    {
        JsonObject add = command[Constant::ADD_KEY];
        Component *construction = ComponentFactory::construct(add);
        q.registerAdd(construction);
    }

    // System remove command
    if (command[Constant::REMOVE_KEY].is<String>())
        q.remove(command[Constant::REMOVE_KEY]);

    // System clear config command
    if (command[Constant::CLEAR_CONFIG_KEY].is<String>())
        q.clearConfig(command[Constant::CLEAR_CONFIG_KEY]);

    // System clearspiffs command
    if (command[Constant::CLEAR_SPIFFS_KEY] == true)
    {
        LittleFS.format();
        ESP.restart();
    }
}

void System::configUpdated()
{
}