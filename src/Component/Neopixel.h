#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Component/Component.h>
#include <Constants/Constant.h>
#include <Helper/JsonHelper.h>

class Neopixel : public Component
{
private:
    // Modes
    static const int MODE_OFF = 0;
    static const int MODE_COLOR = 1;
    static const int MODE_RAINBOW = 2;
    static const int MODE_FADE = 3;
    static const int MODE_CHASE = 4;
    static const int MODE_SHADER = 5;

    // Strip
    Adafruit_NeoPixel *strip = NULL;
    int pin;
    int amount;

    // State
    StaticJsonDocument<1024> state;

    // Rainbow runtime
    int hue;

    void configUpdated();

public:
    Neopixel(String name);

    bool isResultReady();
    JsonObject getResult();

    void loop();
    void command(JsonObject &command);
};